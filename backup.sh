#!/usr/bin/env bash

# IMPORTANT:
# Run the install-little-backup-box.sh script first
# to install the required packages and configure the system.

while true
do
# Specify devices and their mount points
STORAGE_DEV="sdb1"
STORAGE_MOUNT_POINT="/media/storage"
CARD_DEV="sda1"
CARD_MOUNT_POINT="/media/card"

# Set the ACT LED to heartbeat
sudo sh -c "echo heartbeat > /sys/class/leds/led0/trigger"

# Wait for a USB storage device (e.g., a USB flash drive)
STORAGE=$(ls /dev/* | grep $STORAGE_DEV | cut -d"/" -f3)
while [ -z ${STORAGE} ]
  do
  sleep 1
  STORAGE=$(ls /dev/* | grep $STORAGE_DEV | cut -d"/" -f3)
done

# When the USB storage device is detected, mount it
mount /dev/$STORAGE_DEV $STORAGE_MOUNT_POINT

# Set the ACT LED to blink at 1000ms to indicate that the storage device has been mounted
sudo sh -c "echo timer > /sys/class/leds/led0/trigger"
sudo sh -c "echo 1000 > /sys/class/leds/led0/delay_on"

# Wait for a card reader or a camera
CARD_READER=$(ls /dev/* | grep $CARD_DEV | cut -d"/" -f3)
until [ ! -z $CARD_READER ]
  do
  sleep 1
  CARD_READER=$(ls /dev/sd* | grep $CARD_DEV | cut -d"/" -f3)
done

# If the card reader is detected, mount it and obtain its UUID
if [ ! -z $CARD_READER ]; then
  mount /dev/$CARD_DEV $CARD_MOUNT_POINT
  
  # Create the CARD_ID file containing a random 8-digit identifier if doesn't exist
  if [ ! -f $CARD_MOUNT_POINT/CARD_ID ]; then
    < /dev/urandom tr -cd 0-9 | head -c 8 > $CARD_MOUNT_POINT/CARD_ID
  fi
  # Read the 8-digit identifier number from the CARD_ID file on the card
  # and use it as a directory name in the backup path
  read -r ID < $CARD_MOUNT_POINT/CARD_ID

  # Create the backup folder (should be all on the same folder to allow updates on folder)
  BACKUP_PATH=$STORAGE_MOUNT_POINT/"$ID"

  # Perform backup using rsync
  rsync -avhr $CARD_MOUNT_POINT/ $BACKUP_PATH

  # Turn off the ACT LED to indicate that the backup is completed
  sudo sh -c "echo 0 > /sys/class/leds/led0/brightness"
fi

# Shutdown
sync

sudo wall  'Please remove SDCard!'

umount /dev/$CARD_DEV

FILES=$(find /media/storage -type f -name '*.JPG' ! -iname '*_thumb.JPG' )
for f in $FILES
do
	if [ ! -f ${f/.JPG/_thumb.JPG} ]; then
		echo "File not found!"
		convert $f -resize 200x100! ${f/.JPG/_thumb.JPG};
	fi

  echo "Processing $f file..."

done

done