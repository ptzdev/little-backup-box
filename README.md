![Little Backup Box](LogoLBB.png)

Bash shell scripts that transform a Raspberry Pi (or any single-board computer running a Debian-based Linux distribution) into an inexpensive, fully-automatic, pocketable photo backup and streaming device.

## Installation
Download the Image for the Raspberry an put into the Card
    https://downloads.raspberrypi.org/raspbian_lite_latest
    https://www.raspberrypi.org/documentation/installation/installing-images/windows.md
    Enable the SSH by create a File called ssh on card root

Install Git and screen:

    sudo apt install git-core screen

Clone the Little Backup Box Git repository on your Raspberry Pi:

    git clone https://bitbucket.org/PTorrezao/little-backup-box.git

Switch to the *little-backup-box* directory and make the *install-little-backup-box.sh* script executable:

```
cd little-backup-box
chmod +x install-little-backup-box.sh
```

Run the installer script:

    ./install-little-backup-box.sh

Edit /etc/samba/smb.conf and add the following configs
[Photos]
    comment = Photos
    path=/media/storage
    available = yes
    browsable = yes
    writable = yes
    public = yes

Follow setting-up-a-raspberry-pi-as-a-wifi-access-point.pdf for now.

## Usage

1. Boot the Raspberry Pi
2. Plug in the backup storage device
3. Plug in the card reader and wait till the Raspberry Pi shuts down

**Note:** To differentiate between different storage cards, the backup script assigns a random 8-digit identifying number to each card (this number is stored in the *CARD_ID* file in the root of the card). The contents of the card is saved on the storage device in a folder with the identifying number as its name.

# Problems?

Please report bugs and issues in the [Issues](https://gitlab.com/dmpop/little-backup-box/issues) section.

## Author

Dmitri Popov [dmpop@linux.com](mailto:dmpop@linux.com)

## License

The [GNU General Public License version 3](http://www.gnu.org/licenses/gpl-3.0.en.html)


